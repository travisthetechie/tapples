///<reference path='../typings/request/request.d.ts'/>
///<reference path='../typings/es6-promise/es6-promise.d.ts'/>

import * as request from "request";

let baseApiUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=";

export function locate(location : String) : Promise<any> {
	return new Promise<any>((resolve, reject) => {
		request(baseApiUrl + location, (error, response, body) => {
			if (error) {
				reject(error);
			}
			var data = JSON.parse(body);
			resolve(data.results[0].geometry.location);
		});
	});
}