///<reference path='suncalc.d.ts'/>

import * as SunCalc from "suncalc";

export function checkBodies(latitude : number, longitude : number) {
	var when = Date.now();
	var sunLocation = SunCalc.getPosition(when, latitude, longitude);
	var moonLocation = SunCalc.getMoonPosition(when, latitude, longitude);

	var sunHeight = Math.max(sunLocation.altitude, 0.0) / Math.PI / 2.0;
	var moonHeight = Math.max(moonLocation.altitude, 0.0) / Math.PI / 2.0;

	return {
		isSunUp: sunLocation.altitude > 0,
		isMoonUp: moonLocation.altitude > 0,
		sunHeight: sunHeight,
		moonHeight: moonHeight,
	}
}