declare module "suncalc" {
	export function getPosition(date : number, latitude : number, longitude : number) : BodyPosition;
	export function getMoonPosition(date : number, latitude : number, longitude : number) : BodyPosition;
}

interface BodyPosition {
	azimuth : number;
	altitude : number;
	distance? : number; 
}