///<reference path='typings/node/node.d.ts'/>
///<reference path='typings/express/express.d.ts'/>
///<reference path='typings/body-parser/body-parser.d.ts'/>

import * as express from "express";
import * as bodyParser from "body-parser";
import * as geoLocator from "./lib/geo_locator";
import * as bodyLocator from "./lib/body_locator";

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static('public'));

app.get('/geo', (req, res) => {
	var location : String = req.query.location;
	geoLocator.locate(location).then((data) => {
		res.json(data);
	});
});

app.post('/hipchat/geo', (req, res) => {
	var location = req.body.item.message.message.replace('/geocode ', '');
	var payload = {
		color: "green",
		message: "",
		notify: false,
		message_format: "html"
	};

	geoLocator.locate(location).then((data) => {
		payload.message = `Found at (${data.lat}, ${data.lng}).`
		res.json(payload);
	}).catch((error) => {
		payload.color = "red";
		payload.message = "Failed lookup: " + error;
		res.json(payload);
	});
});

app.get('/celestial/bodiesup', (req, res) => {
	var location : String = req.query.location;
	geoLocator.locate(location).then((data) => {
		res.json(bodyLocator.checkBodies(data.lat, data.lng));
	});
	
});

app.post('/hipchat/celestial/bodiesup', (req, res) => {
	var location = req.body.item.message.message.replace('/moonfinder ', '');
	var payload = {
		color: "green",
		message: "",
		notify: false,
		message_format: "html"
	};

	geoLocator.locate(location).then((data) => {
		var bodyMessage = "";
		var bodyLocations = bodyLocator.checkBodies(data.lat, data.lng);
		if (bodyLocations.isSunUp) {
			bodyMessage = "the sun is up";
			if (bodyLocations.isMoonUp) {
				bodyMessage += " and the moon is up";
			}
		} else if (bodyLocations.isMoonUp) {
			bodyMessage = "the moon is up";
		} else {
			bodyMessage = "the sun nor the moon is up";
		}
		
		var url = (process.env.BASE_URL || "http://localhost:3000/") + "?location=" + location;
		var dialogOptions = "{resize:false,width:\"400px\",height:\"600px\",header:\"Moon Finder\",chrome:false}"
		
		payload.message = `I've found <a href="${url}" data-target="dialog" data-target-options="${dialogOptions}">${bodyMessage}</a>.`
		
		res.json(payload);
	});
});

var port = process.env.PORT || 3000;

app.listen(port, function(){
    console.log("Express server listening on port %d in %s mode", port, app.settings.env);
});

export var App = app;