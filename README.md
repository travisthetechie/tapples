# Simple web services on Heroku

## Deployments 

```bash
heroku create tapples
heroku config:set BASE_URL=https://tapples.herokuapp.com/
git push heroku master
heroku ps:scale web=1
```

## Running locally

```
npm install
node_modules/.bin/tsd reinstall
npm run start
```

# License

Licensed under Apache 2.0. See LICENSE.txt for more details.